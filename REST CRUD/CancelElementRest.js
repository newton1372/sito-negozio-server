const  fs = require('fs');
var jsonElement={};
var jsonObj=null;
var request=null;
var response=null;
var resolveFun=null;
var rejectFun=null;
var urlProductsDb="Desktop/sito-negozio-server/DATA/ProductsDb.json";





var init = function(req, resp){
    resp.status="KO";
    request=req;
    response=resp;
    fs.readFile(urlProductsDb,"utf8", function(err,data){
        cancelElement(err,data);
    });
 
}



var saveDB = function(jsonObjFiltered){    
    fs.writeFile(urlProductsDb, JSON.stringify(jsonObjFiltered), "utf8",function(){
        console.log("E' stato cancellato il seguente prodotto\n\n ", jsonElement);
        response.status="OK";
        resolveFun();
    });
}

var cancelElement = function(err,data){
    if(err) rejectFun("ERRORE NELLA LETTURA DEL FILE");	
    else{
        console.log("\n\nRICERCA PRODOTTO DA CODICE...\n");
        jsonObj = JSON.parse(data);
        var indexSelected=null;
        jsonObj.forEach((element, index) => {
            if(element.codice === request.codice){
                indexSelected=index;
                console.log(index)
                jsonElement=element;
            }
        });

        console.log("index selected",indexSelected)
        if(indexSelected===null){
            rejectFun("ELEMENTO NON PRESENTE NEL DATABASE!");
        }
        else{
            console.log("\n\nCANCELLAZIONE PRODOTTO\n");
            var jsonObjFiltered = jsonObj.filter((element)=>{
                console.log(element.codice,",",request.codice);
                return element.codice !== request.codice
            });

            console.log("jsonObjFiltered",jsonObjFiltered)
            saveDB(jsonObjFiltered);
        }
    }
}
              


exports.data = {
     cancelElementExecuter: function(req,resp,resolve,reject){
        resolveFun = resolve;
        rejectFun = reject;
        init(req, resp);
    }
};
