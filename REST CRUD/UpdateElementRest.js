const  fs = require('fs');
var jsonElement={};
var jsonObj=null;
var request=null;
var response=null;
var resolveFun=null;
var rejectFun=null;
//var urlProductsDb="Desktop/sito-negozio-server/DATA/ProductsDb.json";
var urlProductsDb="/Desktop/sito-negozio-server/DATA/ProductsDb.json";





var init = function(req, resp){
    resp.status="KO";
    request=req;
    response=resp;
    fs.readFile(urlProductsDb,"utf8", function(err,data){
        updateElement(err,data);
    });
 
}



var saveDB = function(){    
    fs.writeFile(urlProductsDb, JSON.stringify(jsonObj), "utf8",function(){
        console.log("E' stato aggiornato il seguente prodotto\n\n ", jsonElement);
        response.status="OK";
        resolveFun();
    });
}

var updateElement = function(err,data){
    if(err) rejectFun("ERRORE NELLA LETTURA DEL FILE");	
    else if(!request.codice || !request.nome || !request.categorie|| !request.descrizione) rejectFun("GLI ATTRIBUTI NOME, CODICE E CATEGORIE DEVONO ESSERE VALORIZZATI");
    else{
        console.log("\n\nRICERCA PRODOTTO DA CODICE...\n");
        jsonObj = JSON.parse(data);
        var indexSelected=null;
        jsonObj.forEach((element, index) => {
            if(element.codice === request.codice){
                indexSelected=index;
                jsonElement=element;
            }
        });
        if(indexSelected===null){
            rejectFun("ELEMENTO NON PRESENTE NEL DATABASE!");
        }
        else{
            console.log("\n\nAGGIORNAMENTO PRODOTTO\n");
            jsonObj[indexSelected] =request;
            saveDB();
        }
    }
}
              


exports.data = {
    updateElementExecuter: function(req,resp,resolve,reject){
        resolveFun = resolve;
        rejectFun = reject;
        init(req, resp);
    }
};
