const  fs = require('fs');
var jsonObj=null;
var request=null;
var response=null;
var resolveFun=null;
var rejectFun=null;
var urlProductsDb="Desktop/Progetti/sito-negozio-server/DATA/ProductsDb.json";

var init = function(req, resp){
    resp.status="KO";
    request=req;
    response=resp;
    fs.readFile(urlProductsDb,"utf8", function(err,data){
        readElementByCod(err,data);
    });
 
}



var readElementByCod = function(err,data){
    if(err) rejectFun("ERRORE NELLA LETTURA DEL FILE");	
    else{
        console.log("\n\nESTRAZIONE PRODOTTI...\n");
        jsonObj = JSON.parse(data);
        response.body = jsonObj;
        response.status = "OK";
        resolveFun();                                  
    }
}
              


exports.data = {
    readAllElementsExecuter: function(req,resp,resolve,reject){
        resolveFun = resolve;
        rejectFun = reject;
        init(req, resp);
    }
};
