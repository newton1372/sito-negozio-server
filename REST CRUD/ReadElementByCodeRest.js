const  fs = require('fs');
var jsonObj=null;
var request=null;
var response=null;
var resolveFun=null;
var rejectFun=null;
var urlProductsDb="Desktop/sito-negozio-server/DATA/ProductsDb.json";

var init = function(req, resp){
    resp.status="KO";
    request=req;
    response=resp;
    fs.readFile(urlProductsDb,"utf8", function(err,data){
        readElementByCode(err,data);
    });
 
}



var readElementByCode = function(err,data){
    if(err) rejectFun("ERRORE NELLA LETTURA DEL FILE");	
    else{
        console.log("\n\nRICERCA PRODOTTO DA CODICE...\n");
        jsonObj = JSON.parse(data);
        var element = jsonObj.filter((x)=>{
            return x.codice === request.codice
        })[0];

        if(element===undefined){
            rejectFun("ELEMENTO NON PRESENTE NEL DATABASE!");
        }
        else{
            response.body = element;
            response.status = "OK"
            resolveFun();      
        }                               
    }
}
              


exports.data = {
    readElementByCodeExecuter: function(req,resp,resolve,reject){
        resolveFun = resolve;
        rejectFun = reject;
        init(req, resp);
    }
};
