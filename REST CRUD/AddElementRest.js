const  fs = require('fs');
var jsonElement={};
var jsonObj=null;
var request=null;
var response=null;
var resolveFun=null;
var rejectFun=null;
var urlProductsDb="/home/rosario/Desktop/Progetti/sito-negozio-server/DATA/ProductsDb.json";
//var urlProductsDb="../DATA/ProductsDb.json";

var init = function(req, resp){
    resp.status="KO";
    request=req;
    response=resp;
    fs.readFile(urlProductsDb,"utf8", function(err,data){
        addElement(err,data);
    });
 
}





var manageNewElementAnsw = function(result){
    jsonElement.codice=result.codice;
    jsonElement.nome=result.nome;
    jsonElement.descrizione=result.descrizione;
    jsonElement.brand=result.brand;
    jsonElement.prezzo=result.prezzo;
    jsonElement.immagine1=result.immagine1;
    jsonElement.immagine2=result.immagine2;
    jsonElement.immagine3=result.immagine3;
    jsonElement.dimensioni=result.dimensioni;
    jsonElement.peso=result.peso;
    jsonElement.categorie=result.categorie;
    jsonElement.quantita=result.quantita;
    jsonElement.promozione=result.promozione;
    
    jsonObj.push(jsonElement);
    
    fs.writeFile(urlProductsDb, JSON.stringify(jsonObj), "utf8",function(){
        console.log("E' stato aggiunto il seguente prodotto\n\n ", jsonElement);
        response.status="OK";
        resolveFun();
    });
}

var addElement = function(err,data){
    if(err) rejectFun("ERRORE NELLA LETTURA DEL FILE"+err);	
    else if(!request.codice || !request.nome || !request.categorie||!request.descrizione) {
        console.log(request.codice)
        console.log(request.nome)
        console.log(request.descrizione)
        console.log(request.categorie)
        rejectFun("GLI ATTRIBUTI NOME, DESCRIZIONE, CODICE E CATEGORIE DEVONO ESSERE VALORIZZATI");
    }
    else{
        console.log("\n\nAGGIUNTA NUOVO PRODOTTO\n");
        jsonObj = JSON.parse(data);
        manageNewElementAnsw(request);
    }
}
              


exports.data = {
    addElementExecuter: function(req,resp,resolve,reject){
        resolveFun = resolve;
        rejectFun = reject;
        init(req, resp);
    }
};
